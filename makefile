ASM_FLAGS=-felf64
ASM=nasm

clean:
	rm *.o

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

program: dict.o main.o lib.o
	ld -o $@ $^

.PHONY: clean
