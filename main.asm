%include "lib.inc"
%include "colon.inc"
%include "words.inc"


%define MAX_BUFFER_SIZE 256
%define POINTER_SIZE 8

global _start

section .data
    too_long: db "too long", 0
    %define BUF_SIZE 256
    no_such_key: db "no such key", 0


section .text

extern find_word

_start:
    sub rsp, BUF_SIZE
    mov rdi, rsp
    add rsp, BUF_SIZE
    push rdi
    
    call read_string
    pop rdi
    test rax, rax
    push rax

    jz .too_long
    

    
    mov rsi, pointer
    call find_word
    test rax, rax
    jz .no_such_key
    jmp .print_by_key

    .print_by_key:

        mov rdi, rax
        add rdi, 8
        pop rax
        add rdi, rax
        
        call print_string
        call print_newline
        call exit

    .too_long:
        mov rdi, too_long
        call print_string
        call exit

    .no_such_key:
        mov rdi, no_such_key
        call print_string
        call exit
